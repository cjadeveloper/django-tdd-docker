# TDD with Django, DRF, and Docker

[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pipelinestatus](https://gitlab.com/cjadeveloper/django-tdd-docker/badges/master/pipeline.svg)](https://gitlab.com/cjadeveloper/django-tdd-docker/commits/master)

> An implementation of the [TestDriven.io](https://testdriven.io/) course

Learn how to build, test, and deploy a Django app with Docker, Pytest, and Django REST Framework!

## Learning Objectives

By the end you will be able to:

1. Develop a RESTful API with Python, Django, and Django REST Framework
2. Practice Test-Driven Development
3. Test a Django app with Pytest
4. Containerize Django and Postgres inside a Docker container
5. Run unit and integration tests with code coverage inside a Docker container
6. Create a custom User model in Django
7. Implement an API with Django REST Framework Views and Serializers
8. Check your code for any code quality issues via a linter
9. Configure GitLab CI for continuous integration and deployment
10. Speed up a Docker-based CI build with Docker Cache
11. Deploy Django, Gunicorn, and Postgres to Heroku with Docker
12. Manage static assets with WhiteNoise
13. Create a production Dockerfile that uses multistage Docker builds
14. Document a RESTful API with Swagger/OpenAPI and Core API
15. Parameterize test functions and mock functionality in tests with Pytest
16. Run tests in parallel with pytest-xdist

## Tools and Technologies

1. Python
2. Django
3. Docker
4. Postgres
5. Pytest
6. Django
7. REST Framework
8. Gunicorn
9. Coverage
10. pyflake8
11. Black
12. isort
13. HTTPie
14. GitLab
15. Heroku
16. WhiteNoise
17. Swagger/OpenAPI

## Common Commands

Build the images:

```console
docker-compose build
```

Run the containers:

```console
docker-compose up -d
```

Create migrations:

```console
docker-compose exec movies python manage.py makemigrations
```

Apply migrations:

```console
docker-compose exec movies python manage.py migrate
```

Seed the database:

```console
docker-compose exec movies python manage.py loaddata movies.json
```

Run the tests:

```console
docker-compose exec movies pytest -p no:warnings
```

Run the tests with coverage:

```console
docker-compose exec movies pytest -p no:warnings --cov=.
```

Lint:

```console
docker-compose exec movies flake8 .
```

Run Black and isort with check options:

```console
docker-compose exec movies black --exclude=migrations --check .
docker-compose exec movies /bin/sh -c "isort ./*/*.py --check-only"
```

Make code changes with Black and isort:

```console
docker-compose exec movies black --exclude=migrations .
docker-compose exec movies /bin/sh -c "isort ./*/*.py"
```

## Other Commands

To stop the containers:

```console
docker-compose stop
```

To bring down the containers:

```console
docker-compose down
```

Want to force a build?

```console
docker-compose build --no-cache
```

Remove images:

```console
docker rmi $(docker images -q)
```

## Postgres

Want to access the database via psql?

```console
docker-compose exec movies-db psql -d movies_dev -U movies
```

Then, you can connect to the database and run SQL queries. For example:

```console
> select * from movies_movie;```
