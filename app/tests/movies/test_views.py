import pytest

from movies.models import Movie

# /api/movies       GET	    READ	get all movies
# /api/movies/:id	GET	    READ	get a single movie
# /api/movies	    POST	CREATE	add a movie
#
# For each, we'll:
#
# 1. write a test
# 2. run the test, to ensure it fails (red)
# 3. write just enough code to get the test to pass (green)
# 4. refactor (if necessary)


# ADD a Movie
@pytest.mark.django_db
def test_add_movie(client):
    movies = Movie.objects.all()
    assert len(movies) == 0

    resp = client.post(
        "/api/movies/",
        {"title": "The Godfather", "genre": "drama", "year": "1972"},
        content_type="application/json",
    )
    assert resp.status_code == 201
    assert resp.data["title"] == "The Godfather"

    movies = Movie.objects.all()
    assert len(movies) == 1


# Let's add some tests to ensure the following scenarios are being handled correctly:
#
# - A payload is not sent
# - The payload is invalid -- i.e., the JSON object is empty or it contains the wrong keys
#
@pytest.mark.django_db
def test_add_movie_invalid_json(client):
    movies = Movie.objects.all()
    assert len(movies) == 0

    resp = client.post("/api/movies/", {}, content_type="application/json")
    assert resp.status_code == 400

    movies = Movie.objects.all()
    assert len(movies) == 0


@pytest.mark.django_db
def test_add_movie_invalid_json_keys(client):
    movies = Movie.objects.all()
    assert len(movies) == 0

    resp = client.post(
        "/api/movies/",
        {"title": "The Godfather", "genre": "drama"},
        content_type="application/json",
    )
    assert resp.status_code == 400

    movies = Movie.objects.all()
    assert len(movies) == 0


# GET a Single Movie
#
@pytest.mark.django_db
def test_get_single_movie(client, add_movie):
    movie = add_movie(title="The Godfather", genre="drama", year="1972")
    resp = client.get(f"/api/movies/{movie.id}/")
    assert resp.status_code == 200
    assert resp.data["title"] == "The Godfather"


def test_get_single_movie_incorrect_id(client):
    resp = client.get(f"/api/movies/foo/")
    assert resp.status_code == 404


# GET All Movies
#
# Since we'll have to add a few movies first, let's add a fixture that uses the
# "factory as fixture" pattern to a new file called conftest.py in "app/tests/movies"
@pytest.mark.django_db
def test_get_all_movies(client, add_movie):
    movie_one = add_movie(title="The Godfather", genre="drama", year="1972")
    movie_two = add_movie(title="Pink Floyd The Wall", genre="drama", year="1982")
    resp = client.get(f"/api/movies/")
    assert resp.status_code == 200
    assert resp.data[0]["title"] == movie_one.title
    assert resp.data[1]["title"] == movie_two.title


# DELETE a Movie
#
@pytest.mark.django_db
def test_delete_movie(client, add_movie):
    movie = add_movie(title="The Godfather", genre="drama", year="1972")
    resp = client.get(f"/api/movies/{movie.id}/")
    assert resp.status_code == 200  # OK
    assert resp.data["title"] == movie.title

    resp_two = client.delete(f"/api/movies/{movie.id}/")
    assert resp_two.status_code == 204  # No Content

    resp_three = client.get(f"/api/movies/")
    assert resp_three.status_code == 200  # OK
    assert len(resp_three.data) == 0


@pytest.mark.django_db
def test_delete_movie_incorrect_id(client):
    resp = client.delete(f"/api/movies/999/")
    assert resp.status_code == 404  # Not Found


# UPDATE Movie
#
@pytest.mark.django_db
def test_update_movie(client, add_movie):
    movie = add_movie(title="The Godfather", genre="drama", year="1973")  # Wrong year

    # Correct wrong year
    resp = client.put(
        f"/api/movies/{movie.id}/",
        {"title": "The Godfather", "genre": "drama", "year": "1972"},
        content_type="application/json",
    )
    assert resp.status_code == 200  # OK
    assert resp.data["title"] == "The Godfather"
    assert resp.data["year"] == "1972"

    resp_two = client.get(f"/api/movies/{movie.id}/")
    assert resp_two.status_code == 200
    assert resp_two.data["title"] == "The Godfather"
    assert resp_two.data["year"] == "1972"


@pytest.mark.django_db
def test_update_movie_incorrect_id(client):
    resp = client.put(f"/api/movies/999/")
    assert resp.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize(
    "add_movie, payload, status_code",
    [
        ["add_movie", {}, 400],
        ["add_movie", {"title": "The Big Lebowski", "genre": "comedy"}, 400],
    ],
    indirect=["add_movie"],
)
def test_update_movie_invalid_json(client, add_movie, payload, status_code):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")
    resp = client.put(
        f"/api/movies/{movie.id}/", payload, content_type="application/json",
    )
    assert resp.status_code == status_code
